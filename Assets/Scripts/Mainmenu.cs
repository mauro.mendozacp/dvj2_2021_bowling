﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Mainmenu : MonoBehaviour
{    
    public GameObject menu;
    public GameObject controls;
    public GameObject credits;

    public GameObject panel;
    public float duration;

    private float t = 0f;

    private void Start()
    {
        panel.SetActive(true);
    }

    private void Update()
    {
        if (t <= 1)
        {
            t += Time.deltaTime / duration;

            panel.GetComponent<Image>().color = new Color(0, 0, 0, 1 - t);
        }
    }

    public void PlayBowling()
    {
        SceneManager.LoadScene(GameManager.Instance.BowlingScene);
    }

    public void PlayModeShoot()
    {
        SceneManager.LoadScene(GameManager.Instance.ShootingScene);
    }

    public void OpenCredits()
    {
        credits.SetActive(true);
        menu.SetActive(false);
    }

    public void CloseCredits()
    {
        credits.SetActive(false);
        menu.SetActive(true);
    }

    public void OpenOptions()
    {
        controls.SetActive(true);
        menu.SetActive(false);
    }

    public void CloseOptions()
    {
        controls.SetActive(false);
        menu.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
