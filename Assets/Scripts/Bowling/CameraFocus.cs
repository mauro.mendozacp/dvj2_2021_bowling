﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    private Vector3 distance;
    public GameObject bowling;
    public GameObject ball;
    public GameObject reference;
    public float maxPositionZ;

    // Start is called before the first frame update
    void Start()
    {
        distance = transform.position - ball.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (ball.transform.position.z < maxPositionZ)
        {
            transform.position = ball.transform.position + distance;
        }
        else
        {
            if (!bowling.GetComponent<BowlingGame>().StartBowling)
            {
                bowling.GetComponent<BowlingGame>().StartBowling = true;
            }

            Vector3 aux = new Vector3(0, 0, maxPositionZ - ball.transform.position.z);
            transform.position = ball.transform.position + distance + aux;
        }

        transform.LookAt(ball.transform.position);
    }
}
