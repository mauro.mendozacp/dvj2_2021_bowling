﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BowlingGame : MonoBehaviour
{
    public GameObject pines;
    public GameObject ball;
    public int pinesCount;
    public int pinePoints;
    public float maxRotation;
    public bool StartBowling { get; set; }

    private float timer = 0f;
    private float checkStartTime = 8f;
    private bool startCheckPines;
    private bool allPinesStop;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.GameMode = GameManager.EGameMode.Bowling;
        StartMatch();
    }

    public void Restart()
    {
        pines.GetComponent<PineManager>().Restart();
        StartMatch();
    }

    private void StartMatch()
    {
        StartRound();
        GameManager.Instance.ResetGame();
        ball.GetComponent<Ball>().ShootsCount = ball.GetComponent<Ball>().shootsCountMax;
    }

    private void StartRound()
    {
        StartBowling = false;
        startCheckPines = false;
        allPinesStop = false;
    }

    private void SetGameOver()
    {
        if (ball.GetComponent<Ball>().ShootsCount <= 0)
        {
            GameManager.Instance.GameOver = true;
            GameManager.Instance.WinGame = false;
        }
        if (pinesCount == 0)
        {
            GameManager.Instance.GameOver = true;
            GameManager.Instance.WinGame = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.GameOver)
        {
            if (allPinesStop)
            {
                int pinesDown = 0;
                foreach (GameObject pine in pines.GetComponent<PineManager>().listPines)
                {
                    if (pine.activeSelf)
                    {
                        if (Mathf.Abs(pine.transform.rotation.x) > maxRotation || Mathf.Abs(pine.transform.rotation.z) > maxRotation)
                        {
                            pine.SetActive(false);
                            pinesDown++;
                            GameManager.Instance.Score += pinePoints;
                        }
                    }
                }

                pinesCount -= pinesDown;
                ball.GetComponent<Ball>().Restart();
                StartRound();
                SetGameOver();
            }
            else
            {
                if (StartBowling)
                {
                    timer += Time.deltaTime;

                    if (startCheckPines)
                    {
                        if (timer >= 1)
                        {
                            timer = 0;

                            foreach (GameObject pine in pines.GetComponent<PineManager>().listPines)
                            {
                                if (pine.GetComponent<Rigidbody>().velocity != Vector3.zero)
                                {
                                    return;
                                }
                            }

                            allPinesStop = true;
                        }
                    }
                    else
                    {
                        if (timer >= checkStartTime)
                        {
                            startCheckPines = true;
                            timer = 0f;
                        }
                    }
                }
            }
        }
        else
        {
            SceneManager.LoadScene(GameManager.Instance.GameOverScene);
        }
    }
}
