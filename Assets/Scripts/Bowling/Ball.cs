﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Ball : MonoBehaviour
{
    private Rigidbody rb;
    
    public bool shoot;

    public float SpeedZ { get; set; }
    public Vector3 startPosition;

    public GameObject bowling;
    public GameObject reference;
    public GameObject instancing;
    public float minSpeedZ;
    public float maxSpeedZ;

    public float speedDirX;

    public float speedX;
    public float minPosX;
    public float maxPosX;
    private bool directionLeft;

    public int ShootsCount { get; set; }
    public int shootsCountMax;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        shoot = false;

        SpeedZ = minSpeedZ;
        directionLeft = true;
        ShootsCount = shootsCountMax;
    }

    public void Restart()
    {
        shoot = false;
        transform.localPosition = startPosition;
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        SpeedZ = minSpeedZ;
    }

    void MoveX()
    {
        if (!shoot && !GameManager.Instance.GameOver)
        {
            float auxSpeedX = 0;

            if (directionLeft)
            {
                auxSpeedX = speedX * -1;
            }
            else
            {
                auxSpeedX = speedX;
            }

            Vector3 pos = transform.position;
            pos.x += auxSpeedX * Time.deltaTime;
            transform.position = pos;

            if (transform.position.x <= minPosX)
            {
                directionLeft = false;
            }
            if (transform.position.x >= maxPosX)
            {
                directionLeft = true;
            }
        }

        if (shoot)
        {
            float hor = Input.GetAxis("Horizontal");

            rb.AddForce(reference.transform.right * speedDirX * hor);
        }
    }

    void IncreseForce()
    {
        if (!shoot && !GameManager.Instance.GameOver)
        {
            float ver = Input.GetAxis("Vertical");
            float auxSpeed = SpeedZ + 50 * ver * Time.deltaTime;

            if (auxSpeed >= maxSpeedZ)
            {
                SpeedZ = maxSpeedZ;
            }
            else if (auxSpeed <= minSpeedZ)
            {
                SpeedZ = minSpeedZ;
            }
            else
            {
                SpeedZ = auxSpeed;
            }
        }
    }

    private void Shoot()
    {
        int impulse = 70;
        int porcentExtract = 10;

        if (!shoot && !GameManager.Instance.GameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                shoot = true;
                ShootsCount--;
                rb.constraints = RigidbodyConstraints.None;
                rb.AddForce(reference.transform.forward * (SpeedZ / porcentExtract) * impulse, ForceMode.Impulse);
            }
        }
    }

    private void RandomPines()
    {
        if (!shoot && !GameManager.Instance.GameOver)
        {
            if (Input.GetKey(KeyCode.P))
            {
                instancing.GetComponent<PineManager>().InitRandom();
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveX();
    }

    private void Update()
    {
        RandomPines();
        IncreseForce();
        Shoot();
    }
}
