﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_Bowling : MonoBehaviour
{
    public GameObject ball;
    public GameObject bowGame;

    public TMPro.TMP_Text forceValue;
    public TMPro.TMP_Text pointsValue;
    public TMPro.TMP_Text pinesValue;
    public TMPro.TMP_Text shootsValue;

    // Update is called once per frame
    void Update()
    {
        forceValue.text = ((int)(ball.GetComponent<Ball>().SpeedZ)).ToString();
        pinesValue.text = bowGame.GetComponent<BowlingGame>().pinesCount.ToString();
        shootsValue.text = ball.GetComponent<Ball>().ShootsCount.ToString();
        pointsValue.text = GameManager.Instance.Score.ToString();
    }
}
