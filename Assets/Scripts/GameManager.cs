﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int MainMenuScene { get; } = 0;

    public int BowlingScene { get; } = 1;

    public int ShootingScene { get; } = 2;

    public int GameOverScene { get; } = 3;

    public enum EGameMode
    {
        Bowling,
        Shooting
    }

    public int Score { get; set; }
    public bool GameOver { get; set; }
    public bool WinGame { get; set; }
    public EGameMode GameMode { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ResetGame()
    {
        Score = 0;
        GameOver = false;
        WinGame = false;
    }
}
