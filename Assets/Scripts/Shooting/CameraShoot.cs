﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShoot : MonoBehaviour
{
    [SerializeField] private float mouseSensitive;

    [SerializeField] private float rotLimitY;

    [SerializeField] private Transform player, gun;

    private float yAxis;
    private const float rotMax = 360;

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotX = mouseX * mouseSensitive;
        float rotY = mouseY * mouseSensitive;

        yAxis -= rotY;

        Vector3 rotPlayer = player.transform.rotation.eulerAngles;
        Vector3 rotGun = gun.transform.rotation.eulerAngles;

        rotGun.x -= rotY;
        rotGun.z = 0;
        rotPlayer.y += rotX;

        if (yAxis > rotLimitY)
        {
            yAxis = rotLimitY;
            rotGun.x = rotLimitY;
        }
        else if (yAxis < -rotLimitY)
        {
            yAxis = -rotLimitY;
            rotGun.x = rotMax - rotLimitY;
        }

        gun.rotation = Quaternion.Euler(rotGun);
        player.rotation = Quaternion.Euler(rotPlayer);
    }
}