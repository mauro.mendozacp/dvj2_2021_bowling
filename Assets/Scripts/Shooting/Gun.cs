﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngineInternal;

public class Gun : MonoBehaviour
{
    private Camera cam;

    [SerializeField] private float maxDistance;
    [SerializeField] private string objectiveLayer;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Shoot();
    }

    void Shoot()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(mousePos);

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, maxDistance))
            {
                string layerhitted = LayerMask.LayerToName(hit.transform.gameObject.layer);

                if (layerhitted == objectiveLayer)
                {
                    hit.transform.gameObject.GetComponent<Pine>().Explosion();
                }
                else
                {
                    GameManager.Instance.GameOver = true;
                    GameManager.Instance.WinGame = false;
                }
            }
        }
    }
}
