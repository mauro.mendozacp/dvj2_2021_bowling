﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_Shooting : MonoBehaviour
{
    public TMPro.TMP_Text pointsValue;

    // Update is called once per frame
    void Update()
    {
        pointsValue.text = GameManager.Instance.Score.ToString();
    }
}
