﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDuration : MonoBehaviour
{
    private float timer;
    private float duration = 5f;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > duration)
        {
            Destroy(gameObject);
        }
    }
}
