﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pine : MonoBehaviour
{
    [SerializeField] private int points;

    [SerializeField] private float explosionSize;
    [SerializeField] private GameObject explosion;

    [SerializeField] private int generatedPinesCount;
    [SerializeField] private float miniPineSize;
    [SerializeField] private float miniPineMass;
    [SerializeField] private float miniPineForceUp;
    private const float porcentExtract = 10;

    public void Explosion()
    {
        GameManager.Instance.Score += points;

        GameObject e = Instantiate(explosion, transform.position, Quaternion.identity).gameObject;
        e.name = name + " explosion";
        e.transform.localScale *= explosionSize;
        e.transform.parent = GameObject.FindGameObjectWithTag("ExplosionList").transform;

        for (int i = 0; i < generatedPinesCount; i++)
        {
            GameObject p = Instantiate(gameObject, transform.position, Quaternion.identity).gameObject;
            p.AddComponent<ExplosionDuration>();
            p.layer = 0;
            p.name = "Mini Pine " + (i + 1);
            p.transform.localScale *= miniPineSize;
            p.transform.parent = e.transform;
            Vector3 forceDirection = (Vector3.up + (Random.Range(0, 2) == 0 ? Vector3.left : Vector3.right)) * (miniPineForceUp / porcentExtract);
            p.GetComponent<Rigidbody>().mass = miniPineMass;
            p.GetComponent<Rigidbody>().AddForce(forceDirection, ForceMode.Impulse);
        }

        transform.localPosition = GameObject.FindGameObjectWithTag("PineManager").GetComponent<PineManager>().GetRandomPositionPine();
    }
}
