﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootingGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.GameMode = GameManager.EGameMode.Shooting;
        GameManager.Instance.ResetGame();
    }

    // Update is called once per frame
    void Update()
    {
        GameOver();
    }

    void GameOver()
    {
        if (GameManager.Instance.GameOver)
        {
            SceneManager.LoadScene(GameManager.Instance.GameOverScene);
        }
    }
}
