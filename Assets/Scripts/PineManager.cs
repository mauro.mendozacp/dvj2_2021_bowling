﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PineManager : MonoBehaviour
{
    public enum GameMode
    {
        Bowling,
        Shooting
    }

    public GameObject bowlingGame;
    public GameObject prefabPine;
    public int pineLenght;
    public Vector3 initialPosition;
    public Vector3 minPosition;
    public Vector3 maxPosition;
    public float offsetX = 5;
    public float offsetY = 5;
    public PineManager.GameMode gameMode;

    public List<GameObject> listPines = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        if (gameMode == GameMode.Bowling)
        {
            Init();
        }
        if (gameMode == GameMode.Shooting)
        {
            for (int i = 0; i < pineLenght; i++)
            {
                GameObject pine = Instantiate(prefabPine).gameObject;
                pine.name = "Pine " + (listPines.Count + 1);

                pine.transform.parent = transform;
                listPines.Add(pine);
            }

            InitRandom();
        }
    }

    public void Init()
    {
        Vector3 pos = initialPosition;
        Vector3 auxPosInit = pos;
        int col = 4;
        int fil = 4;
        float offsetFil = offsetX / 2;

        for (int i = 0; i < fil; i++)
        {
            for (int j = 0; j < col; j++)
            {
                GameObject pine = Instantiate(prefabPine).gameObject;
                pine.name = "Pine " + (listPines.Count + 1);

                pine.transform.parent = transform;

                auxPosInit.x = initialPosition.x + (offsetFil * i);

                pos = new Vector3(auxPosInit.x + (offsetX * j), auxPosInit.y, auxPosInit.z - (offsetY * i));

                listPines.Add(pine);
                pine.transform.position = pos;
            }
            col--;
        }

        bowlingGame.GetComponent<BowlingGame>().pinesCount = listPines.Count;
    }

    public void InitRandom()
    {
        foreach (GameObject pine in listPines)
        {
            if (pine.activeSelf)
            {
                pine.transform.localPosition = GetRandomPositionPine();
                pine.GetComponent<Rigidbody>().velocity = Vector3.zero;
                pine.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                pine.transform.rotation = prefabPine.transform.rotation;
            }
        }
    }

    public Vector3 GetRandomPositionPine()
    {
        Vector3 auxPos = Vector3.zero;
        bool repeat = false;

        do
        {
            repeat = false;

            auxPos.x = Random.Range(minPosition.x, maxPosition.x);
            auxPos.y = minPosition.y;
            auxPos.z = Random.Range(minPosition.z, maxPosition.z);

            foreach (GameObject p in listPines)
            {
                Vector3 auxMinP = new Vector3(p.transform.localPosition.x - offsetX / 2, minPosition.y, p.transform.localPosition.z - offsetY / 2);
                Vector3 auxMaxP = new Vector3(p.transform.localPosition.x + offsetX / 2, minPosition.y, p.transform.localPosition.z + offsetY / 2);

                if (auxPos.x >= auxMinP.x && auxPos.x <= auxMaxP.x)
                {
                    if (auxPos.z >= auxMinP.z && auxPos.z <= auxMaxP.z)
                    {
                        repeat = true;
                    }
                }
            }

        } while (repeat);

        return auxPos;
    }

    public void Restart()
    {
        foreach (GameObject p in listPines)
        {
            Destroy(p);
        }
        listPines.Clear();

        Init();
    }
}
