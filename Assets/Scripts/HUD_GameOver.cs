﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class HUD_GameOver : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text scoreValue;
    [SerializeField] private GameObject winText;
    [SerializeField] private GameObject loseText;
    [SerializeField] private GameObject finishText;

    void Start()
    {
        if (GameManager.Instance.GameMode == GameManager.EGameMode.Shooting)
        {
            Cursor.lockState = CursorLockMode.None;
            finishText.SetActive(true);
        }
        else
        {
            if (GameManager.Instance.WinGame)
            {
                winText.SetActive(true);
            }
            else
            {
                loseText.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        scoreValue.text = GameManager.Instance.Score.ToString();
    }

    public void RestartGame()
    {
        switch (GameManager.Instance.GameMode)
        {
            case GameManager.EGameMode.Bowling:
                SceneManager.LoadScene(GameManager.Instance.BowlingScene);
                break;
            case GameManager.EGameMode.Shooting:
                SceneManager.LoadScene(GameManager.Instance.ShootingScene);
                break;
            default:
                SceneManager.LoadScene(GameManager.Instance.MainMenuScene);
                break;
        }
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(GameManager.Instance.MainMenuScene);
    }
}
